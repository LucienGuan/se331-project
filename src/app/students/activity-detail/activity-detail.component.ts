import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivityRestImplService } from 'src/app/service/activity-rest-impl.service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.css']
})
export class ActivityDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private activityService: ActivityRestImplService) { }
activity : Activity;
  ngOnInit() {
    this.route.params
    .subscribe((params: Params) => {
    this.activityService.getActivity(+params['id'])
    .subscribe((input: Activity) => this.activity = input);
    });
  }
  Enroll(act : Activity){
    console.log(act);
    if(confirm("Are you sure to enroll "+act.activityName)) {
      this.activityService.enrollActivity(act)
      .subscribe(() => {
      // this.router.navigate(['./admin/registered'])
    }, (error)=> {
      alert('could not save the value' + error)
    }
    )
    }
  }
}
