import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { EnrolledTableDataSource, EnrolledTableItem } from './enrolled-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity-service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-enrolled-table',
  templateUrl: './enrolled-table.component.html',
  styleUrls: ['./enrolled-table.component.css']
})
export class EnrolledTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<EnrolledTableItem>;
  dataSource: EnrolledTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityName', 'activityTime', 'ActivityDate', 'activityLocation', 'periodRegistration', 'activityDescription', 'hostTeacher','Status','Delete'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor (private activityService: ActivityService){}
  ngOnInit() {
    this.activityService.getEnrollActivity()
      .subscribe(enroll => {
    this.dataSource = new EnrolledTableDataSource();
    this.dataSource.data = enroll;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
    this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
  });
    
}
  ngAfterViewInit() {
    
  }
}
