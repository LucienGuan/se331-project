import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredActivitiyListComponent } from './registered-activitiy-list.component';

describe('RegisteredActivitiyListComponent', () => {
  let component: RegisteredActivitiyListComponent;
  let fixture: ComponentFixture<RegisteredActivitiyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisteredActivitiyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredActivitiyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
