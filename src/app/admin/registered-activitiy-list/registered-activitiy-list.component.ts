import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity-service';


@Component({
  selector: 'app-registered-activitiy-list',
  templateUrl: './registered-activitiy-list.component.html',
  styleUrls: ['./registered-activitiy-list.component.css']
})
export class RegisteredActivitiyListComponent implements AfterViewInit, OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  displayedColumns = ['id', 'activityName', 'activityTime', 'activityDate', 'activityLocation', 'periodRegistration', 'activityDescription', 'hostTeacher'];
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService) { }
  ngOnInit() {
    this.activityService.getActivities()
      .subscribe(activities => {
        this.dataSource = new ActivityTableDataSource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.activities = activities;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
