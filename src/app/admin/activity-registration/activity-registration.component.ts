import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-activity-registration',
  templateUrl: './activity-registration.component.html',
  styleUrls: ['./activity-registration.component.css']
})

export class ActivityRegistrationComponent implements OnInit {

  activities: Activity[];

  constructor(private fb: FormBuilder, private activityService: ActivityService, private router: Router) {
  }
    form = this.fb.group({
      activityName: [''],
      activityTime: [''],
      activityDate: [''],
      activityLocation: [''],
      periodRegistration: [''],
      activityDescription: [''],
      hostTeacher: [''],
      id: [''],
      Status : ['Pending']
  })

  submit() {
    this.activityService.saveActivity(this.form.value)
    .subscribe((activity) => {
      this.router.navigate(['./admin/registered']);
      console.log(this.form.value);
      
    }, (error)=> {
      alert('could not save the value' + error)
    }
    )
  }
  ngOnInit() {
  }

}
