import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup, FormGroupDirective, NgForm } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { Router } from '@angular/router';
//import {MatDatepickerModule} from '@angular/material/datepicker';
import { ErrorStateMatcher } from '@angular/material';
import Student from 'src/app/entity/student';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}
@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  students: Student[];
  startDate = new Date(1995, 0, 1);
  email = new FormControl('', [Validators.required, Validators.email]);
  matcher = new MyErrorStateMatcher();
  // form: FormGroup;

  constructor(private fb: FormBuilder, private studentService: StudentService, private router: Router) {
  }
  
    form = this.fb.group({
    id: [''],
    studentEmail: [''],
    studentFirstname: [''],
    studentSurname: [''],
    studentDob: [''],
    studentId: [''],
    studentImage: [''],
    studentPassword: ['', [Validators.required]],
    studentConfirmpassword: ['']
  }, {validator: this.checkPasswords })

  submit() {
    this.studentService.saveStudent(this.form.value)
    .subscribe((student) => {
      this.router.navigate(['./student']);
    }, (error)=> {
      alert('could not save the value')
    }
    )
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('studentEmail') ? 'Not a valid email' :
            '';
  }

  checkPasswords(form: FormGroup) { // here we have the 'passwords' group
    let pass = form.controls.studentPassword.value;
    let confirmPass = form.controls.studentConfirmpassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }
  
  ngOnInit() {
  }

}
