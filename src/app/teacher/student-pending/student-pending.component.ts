import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';

import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { StudentService } from 'src/app/service/student-service';
import { StudentPendingDataSource } from './student-pending.datasource'

@Component({
  selector: 'app-student-pending',
  templateUrl: './student-pending.component.html',
  styleUrls: ['./student-pending.component.css']
})
export class StudentPendingComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: StudentPendingDataSource;

  displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'confirm'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(students => {
        this.dataSource = new StudentPendingDataSource();
        this.dataSource.data = students;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }

  ngAfterViewInit() {
  }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  Comfirm(student: Student){
    console.log(student);
    if(confirm("CONFIRM "+student.studentFirstname)) {
    }
  }

  Reject(student: Student){
    console.log(student);
    if(confirm("REJECT "+student.studentFirstname)) {
    }
  }


  


}
