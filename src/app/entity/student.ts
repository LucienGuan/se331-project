export default class Student {
  id: number;
  studentEmail: string;
  studentFirstname: string;
  studentSurname: string;
  studentDob: string;
  studentId: string;
  studentImage: string;
  studentPassword: string;
  studentConfirmpassword: string;
  isPending: boolean;
}
