export default class Activity {
  activityName: string;
  activityTime: string;
  activityDate: Date;
  activityLocation: string;
  periodRegistration: string;
  activityDescription: string;
  hostTeacher: string;
  id: number;
  Status: String;
  }